/*********************************************************************
** Program name: menu.cpp
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Menu class implementation file.
** Implements the menu of the game
*********************************************************************/

#include "menu.h"

// Constructor
Menu::Menu()
{
	initialInput = 0;
	buyAnimalInput = 0;
	endOfDayInput = 0;
}


/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 28-09-2018
** Description: Function called getChoiceVar()
** This function is used to display customized information for each variable
** and gets the input and puts it into the Menu class member variables.
******************************************************************************/
int Menu::getChoiceVar(string inputMessage, int minNum, int maxNum, bool extraError, bool quitFlag, bool animalBuyQuitFlag)
{
	int choice = 0;

	cout << "Please enter the information requested below: " << endl;
	cout << inputMessage << endl;

	// To get only 1 option
	cin >> setw(1) >> choice;

	// For user to quit
	if(quitFlag)
	{
		if(choice == 2)
		{
			return 2;
		}
	}

	// For animal buying method
	if(animalBuyQuitFlag)
	{
		if(choice == 4)
		{
			return 0;
		}
	}
	// Error check
	while(((!cin.good()) || (choice < minNum) || (choice > maxNum)))
	{
		// Report problem
		cout << "ERROR!" << endl;
		if(extraError)
		{
			// Any extra error message can be shown here by turning on the extraError bool flag in the function
		}
		cout << inputMessage << endl;

		// Clear stream
		cin.clear();
		cin.ignore(INT_MAX, '\n');

		// Get input again
		cout << "Enter Choice: ";
		cin >> setw(1) >> choice;
	}

	// Clear stream
	cin.clear(); // Clear buffer
	cin.ignore(INT_MAX, '\n'); // Ignore characters and newline

	return choice;
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 28-09-2018
** Description: Function called getUserInput()
** This function gets the user input and stores it in the member variables
** of this class.
******************************************************************************/

bool Menu::getUserInput()
{
	// Same pattern followed for each member variable in this function
	// Please see comments for first one
	// Number of rounds
	// Custom input message to send into the getChoiceVar method
	string inputMessage = "Enter 1 to play Zoo Tycoon and 2 to quit.";
	// Set min and max
	initialInput = getChoiceVar(inputMessage, 1, 2, false, true, false);
	if(initialInput == 0)
	{
		return false;
	}
	// Check for min and max again
	while((1 > initialInput) || (initialInput> 10))
	{
		initialInput = getChoiceVar(inputMessage, 1, 2, false, true, false);
	}

	// Check to see if user wants to quit
	if(initialInput == 2)
	{
//		quitProgram();
		return false;
	}

	return true;

}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 28-09-2018
** Description: Function called quitProgram()
** Used to quit the program if the user so chooses in the initial menu.
******************************************************************************/

void Menu::quitProgram()
{
	cout << "Quitting program. Bye!" << endl;
	std::exit(0);
}

// Get input for buying animal
int Menu::userInputBuyAnimal()
{
	// Same pattern followed for each member variable in this function
	// Please see comments for first one
	// Number of rounds
	// Custom input message to send into the getChoiceVar method
	string inputMessage = "Would you like to buy an animal?\n";
	inputMessage += "1 for Tiger (cost $10,000)\n";
	inputMessage += "2 for Penguin (cost $1,000)\n";
	inputMessage += "3 for Turtle (cost $100)\n";
	inputMessage += "4 for no...\n";
	// Set min and max
	buyAnimalInput = getChoiceVar(inputMessage, 1, 4, false, false, true);
	if(buyAnimalInput == 0)
	{
		// User wants to move to the next day
		return 0;
	}
	else
	{
		// Check for min and max again
		while((1 > buyAnimalInput) || (buyAnimalInput > 4))
		{
			buyAnimalInput = getChoiceVar(inputMessage, 1, 4, false, false, true);
		}

	}

	// Check to see if user wants to quit
	if(buyAnimalInput == 4)
	{
		// Continue onto the next day
		cout << "Okay... you don't want to add any animals to the zoo." << endl;
		return 0;
	}

	if(buyAnimalInput > 0 && buyAnimalInput < 5)
	{
		return buyAnimalInput;
	}

	return buyAnimalInput;
}

// Getter
int Menu::getBuyAnimalInput()
{
	return buyAnimalInput;
}

// End of day
bool Menu::userInputEndOfDay()
{
	// Same pattern followed for each member variable in this function
	// Please see comments for first one
	// Number of rounds
	// Custom input message to send into the getChoiceVar method
	string inputMessage = "Would you like to continue to play or quit the game?\n";
	inputMessage += "1 to continue\n";
	inputMessage += "2 to quit\n";
	// Set min and max
	endOfDayInput = getChoiceVar(inputMessage, 1, 2, false, true, false);
	if(endOfDayInput == 2)
	{
		return false;
	}
	else
	{
		// Check for min and max again
		while((1 > endOfDayInput) || (endOfDayInput > 2))
		{
			endOfDayInput = getChoiceVar(inputMessage, 1, 2, false, true, false);
		}
	}

	// Check to see if user wants to quit
	if(endOfDayInput == 2)
	{
//		quitProgram();
		return false;
	}

	if(endOfDayInput == 1)
	{
		// Continue to the next day
		return true;
	}

	return true;
}

// Getter
// Getter
int Menu::getEndOfDayInput()
{
	return endOfDayInput;
}