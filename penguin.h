/*********************************************************************
** Program name: Penguin.h
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Specification file for Penguin class.
** Child of the Animal class.
*********************************************************************/

#ifndef ZOOTYCOON_PENGUIN_H
#define ZOOTYCOON_PENGUIN_H
#include "animal.h"

class Penguin : public Animal
{

public:
	// Default constructor
	Penguin();

	// Optional constructor
	Penguin(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// Accessors
	int getAge();
	double getCost();
	double getNumBabies();
	double getBaseFoodCost();
	double getPayOff();

	// Destructor
	~Penguin();

};



#endif //ZOOTYCOON_PENGUIN_H
