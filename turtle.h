/*********************************************************************
** Program name: Turtle.h
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Specification file for Turtle class.
** Child of the Animal class.
*********************************************************************/

#ifndef ZOOTYCOON_TURTLE_H
#define ZOOTYCOON_TURTLE_H
#include "animal.h"

class Turtle : public Animal
{

public:
	// Default constructor
	Turtle();

	// Optional constructor
	Turtle(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// Accessors
	int getAge();
	double getCost();
	double getNumBabies();
	double getBaseFoodCost();
	double getPayOff();

	// Destructor
	~Turtle();

};



#endif //ZOOTYCOON_TURTLE_H
