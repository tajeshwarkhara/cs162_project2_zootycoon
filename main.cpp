#include <iostream>
#include "animal.h"
#include "tiger.h"
#include "penguin.h"
#include "turtle.h"
#include "AnimalArray.h"
#include "zoo.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
	Zoo z1;

	z1.playGame();

	return 0;
}