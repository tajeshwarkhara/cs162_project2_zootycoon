/*********************************************************************
** Program name: zoo.h
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Specification file for the Zoo class.
** This class contains most of the running of the game.
*********************************************************************/
#ifndef ZOOTYCOON_ZOO_H
#define ZOOTYCOON_ZOO_H

#include <iostream>
#include <cstdlib> // for srand
#include <ctime>	// for time
#include "animal.h"
#include "tiger.h"
#include "penguin.h"
#include "turtle.h"
#include "AnimalArray.h"
#include "menu.h"
using std::cout;
using std::endl;

class Zoo
{
private:

	Menu m;

	AnimalArray tigerArr;
	AnimalArray penguinArr;
	AnimalArray turtleArr;

	// Key variables
	double playerMoney;
	double totalFeedingCost;
	double dailyFeedingCost = 0;
	double dailyPayOff = 0;
	int days;
	double tigerBonus = 0;
	double dailyProfitLoss = 0;

	// Food costs
	const double baseFoodCost = 10.0;
	const double tigerCost = 10000.0;
	const double penguinCost = 1000.0;
	const double turtleCost = 100.0;

	// Adult age
	const int adultAge = 3;

	// Number of babies
	const int numBabiesTiger = 1;
	const int numBabiesPenguin = 5;
	const int numBabiesTurtle = 10;

	// Feeding costs
	double tigerFeedingCost;
	double penguinFeedingCost;
	double turtleFeedingCost;

	// PayOff
	const double tigerPayOff = .2 * tigerCost;
	const double penguinPayOff = .10 * penguinCost;
	const double turtlePayOff = .5 * turtleCost;

	// Play
	bool play;

public:
	// Default constructor
	Zoo();
	// Destructor
	~Zoo();

	// Getters
	AnimalArray getTigerArr();
	double getPlayerMoney();
	double getDailyFeedingCost();
	double getDailyPayOff();

	// Add tiger to array
	void addTiger();

	// Add tiger to array with arguments
	void addTiger(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// print Tiger Array
	void printTigerArr();

	// Add penguin to array
	void addPenguin();

	// Add penguin to array with arguments
	void addPenguin(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// print penguin Array
	void printPenguinArr();

	// Add turtle to array
	void addTurtle();

	// Add penguin to array with arguments
	void addTurtle(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// print turtle Array
	void printTurtleArr();

	// Buy tiger
	void buyTiger();

	// Buy penguin
	void buyPenguin();

	// Buy turtle
	void buyTurtle();

	// Turn
	int turn();

	// Random events
	void randomEvent();

	// End of day buying
	bool endOfDayBuying();

	// Display profit and loss
	void displayProfitLoss();

	// Pay feeding costs
	void payFeedingCosts();

	// Increase the age of all animal
	void increaseAge();

	// Display morning game stats
	void displayGameStatsMorning();

	// Increase day
	void increaseDay();

	// Play game
	int playGame();

	// Sickness function
	void sickness();

	// Boom function
	void boom();

	// Baby function
	void baby();

	// Calc daily payoff
	void calcDailyPayoff();

	// Calc daily profit/loss
	void calcDailyProfitLoss();

	// Reset variables
	void resetVars();

	// Day separator
	void daySeparator();


};


#endif //ZOOTYCOON_ZOO_H
