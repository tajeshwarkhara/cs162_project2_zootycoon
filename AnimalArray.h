/*********************************************************************
** Program name: AnimalArray.h
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Specification file for AnimalArray class
*********************************************************************/
#ifndef ZOOTYCOON_ANIMALARRAY_H
#define ZOOTYCOON_ANIMALARRAY_H

#include "animal.h"
#include "tiger.h"
#include "penguin.h"
#include "turtle.h"
#include <iostream>
using std::cout;
using std::endl;


class AnimalArray
{
private:
	unsigned int cap; // capacity of the array
	unsigned int nrOfEl; // number of valid elements in the array
	Animal** arr; // pointer pointer array to hold pointers to animal objects and children objects

	void expand(); // To expand the array

public:
	// constructor
	AnimalArray();

	// destructor
	~AnimalArray();

	// Getters
	Animal** getArr();

	// To get the size of the array (till number of valid elements)
	unsigned int getSize();

	// Add an animal object to the array
	void add(Animal el);

	// Add an animal object to the array
	void add(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// To return the selected element from the array
	Animal& getAt(unsigned int index) const;

	// To return the pointer in the Array
	Animal* getAtPointer(unsigned int index);

	// pop the last element of the array
	void popBack();

	//print till number of valid elements
	void print();

	// To return the selected element form the array
	Animal &operator[](unsigned int index);

	// Initialize
	void initialize(int from);
};


#endif //ZOOTYCOON_ANIMALARRAY_H
