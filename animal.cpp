/*********************************************************************
** Program name: animal.cpp
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Implementation file for the Animal class.
** Animal is the parent class to all the animals in the zoo.
*********************************************************************/
#include "animal.h"

// Default constructor
Animal::Animal()
{
	// Initialize everything to 0
	age = 0;
	cost = 0;
	numberOfBabies = 0;
	baseFoodCost = 0;
	payOff = 0;
}

// Optional constructor
Animal::Animal(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
{
	this->age = ageIn;
	this->cost = costIn;
	this->numberOfBabies = numBabiesIn;
	this->baseFoodCost = baseFoodCostIn;
	this->payOff = payOffIn;
}

// Copy constructor
Animal::Animal(const Animal& obj)
{
	age = obj.age;
	cost = obj.cost;
	numberOfBabies = obj.numberOfBabies;
	baseFoodCost = obj.baseFoodCost;
	payOff = obj.payOff;
}
// Overloading the = operator
Animal& Animal::operator=(const Animal& right)
{
	age = right.age;
	cost = right.cost;
	numberOfBabies = right.numberOfBabies;
	baseFoodCost = right.baseFoodCost;
	payOff = right.payOff;
	return *this;
}


// Destructor
Animal::~Animal()
{
	// Deallocate any dynamically allocated memory here
}

// Overloaded stream insertion operator for printing objects
ostream &operator<<(ostream& out, Animal a)
{
	out << "\n Age: " << a.age
		<< "\n Cost: " << a.cost
		<< "\n The number of babies it can have: " << a.numberOfBabies
		<< "\n The base food cost: " << a.baseFoodCost
		<< "\n Payoff: " << a.payOff
		<< "\n";
	return out;
}

// Increase age
void Animal::increaseAge()
{
	age++;
}

// Getters
double Animal::getBaseFoodCost()
{
	return baseFoodCost;
}

// Getter
int Animal::getAge()
{
	return age;
}

// Getter
double Animal::getPayOff()
{
	return payOff;
}