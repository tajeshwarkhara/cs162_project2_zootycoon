/*********************************************************************
** Program name: Turtle.cpp
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Implementation file for Turtle class.
** Child of the Animal class.
*********************************************************************/
#include "turtle.h"

// Default constructor
Turtle::Turtle()
		: Animal(1, 100, 10, 5, 5)
{
	// If a Turtle object is created using a default constructor
	// with no arguments then initialize the following values for
	// that object as default
	// age = 0, cost = 100, nuBabies = 5, baseFoodCost = 5, payOff = .25
}

// Optional constructor
Turtle::Turtle(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
		: Animal(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn)
{
	// Initialized custom values sent in through the constructor
}

// Destructor
Turtle::~Turtle()
{
	// Deallocate any used dynamic memory here
}

// Accessors
int Turtle::getAge()
{
	return age;
}
double Turtle::getCost()
{
	return cost;
}
double Turtle::getNumBabies()
{
	return numberOfBabies;
}
double Turtle::getBaseFoodCost()
{
	return baseFoodCost;
}
double Turtle::getPayOff()
{
	return payOff;
}