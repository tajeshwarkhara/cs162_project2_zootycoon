/*********************************************************************
** Program name: zoo.cpp
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Implementation file for the Zoo class.
** This class contains most of the running of the game.
*********************************************************************/
#include "zoo.h"

// Default constructor
Zoo::Zoo()
{


	// Initializing key values

	// Days
	// Start at day 0
	days = 0;
	dailyProfitLoss = 0;
	dailyFeedingCost = 0;
	dailyPayOff = 0;
	tigerBonus = 0;

	// Money the player starts out with
	playerMoney = 100000;

	// Tiger bonus intialize to 0
	tigerBonus = 0;

	// Daily feeding costs
	tigerFeedingCost = 5*baseFoodCost;
	penguinFeedingCost = baseFoodCost;
	turtleFeedingCost = 0.5 * baseFoodCost;

	// Player starts with 1 Tiger, 1 Penguin, and 1 Turtle
	// Add one animal each to the three arrays
	// At the start the user buys one animal of each type
	// p&l - animal costs subtracted from player money
	buyTiger();
	buyPenguin();
	buyTurtle();



	// For seeding the random event
	srand(time(0));

	// To play
	play = true;


}

// Destructor
Zoo::~Zoo()
{
	// No dynamic memory allocated in the Zoo object.
}

// Getters
AnimalArray Zoo::getTigerArr()
{
	return tigerArr;
}

// Getter
double Zoo::getPlayerMoney()
{
	return playerMoney;
}

// Getter
double Zoo::getDailyFeedingCost()
{
	return dailyFeedingCost;
}

// Getter
double Zoo::getDailyPayOff()
{
	return dailyPayOff;
}

// Add tiger to array
void Zoo::addTiger()
{
	tigerArr.add(Tiger());
}

// Add tiger to array with arguments
void Zoo::addTiger(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
{
	tigerArr.add(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn);
}

// Add penguin
void Zoo::addPenguin()
{
	penguinArr.add(Penguin());
}

// Add penguin to array with arguments
void Zoo::addPenguin(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
{
	penguinArr.add(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn);
}

// Add turtle
void Zoo::addTurtle()
{
	turtleArr.add(Turtle());
}

// Add turtle to array with arguments
void Zoo::addTurtle(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
{
	turtleArr.add(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn);
}

// print Tiger Array
void Zoo::printTigerArr()
{
	for(size_t i = 0; i < tigerArr.getSize(); i++)
	{
		cout << "Tiger: " << i << endl;
		cout << tigerArr.getAt(i) << endl;
	}
}

// print penguin Array
void Zoo::printPenguinArr()
{
	for(size_t i = 0; i < penguinArr.getSize(); i++)
	{
		cout << "Penguin: " << i << endl;
		cout << penguinArr.getAt(i) << endl;
	}
}

// print turtle Array
void Zoo::printTurtleArr()
{
	for(size_t i = 0; i < turtleArr.getSize(); i++)
	{
		cout << "Turtle: " << i << endl;
		cout << turtleArr.getAt(i) << endl;
	}
}

// Buy tiger
// Only used in the day 0 buying
void Zoo::buyTiger()
{
	// Add the tiger to the array
	addTiger();

	// Deduct cost from player money
	playerMoney -= tigerCost;
	dailyFeedingCost += tigerFeedingCost;
	dailyPayOff += tigerPayOff;
}

// Buy penguin
// Only used in the day 0 buying
void Zoo::buyPenguin(){
	// Add the penguin to the array
	addPenguin();

	// Deduct cost from player money
	playerMoney -= penguinCost;
	dailyFeedingCost += penguinFeedingCost;
	dailyPayOff += penguinPayOff;
}

// Buy turtle
// Only used in the day 0 buying
void Zoo::buyTurtle(){
	// Add the turtle to the array
	addTurtle();

	// Deduct cost from player money
	playerMoney -= turtleCost;
	dailyFeedingCost += turtleFeedingCost;
	dailyPayOff += turtlePayOff;
}

// Turn
// After the initial setup this function deals with implementing turns
int Zoo::turn()
{


	bool nextDay = true;

	while(nextDay)
	{

		// Display day separator
		daySeparator();

		// Reset daily variables
		void resetVars();

		// Display game stats morning
		displayGameStatsMorning();

		// Increase the age of all the animals
		increaseAge();

		// Pay for the feeding costs
		payFeedingCosts();

		// Random event takes place
		randomEvent();

		// Increase the day
		increaseDay();

		// Calc daily payoff
		// value of dailyPayoff is updated with the daily payoff of all animals
		//  Does not include the tiger bonus (that in included in the randomEvent() method.

		calcDailyPayoff();

		// Calculate daily profit and loss
		calcDailyProfitLoss();

		// Display the profit and loss
		displayProfitLoss();

		// End of day buying
		bool continueNextDay = endOfDayBuying();
		if(!continueNextDay)
		{
			cout << "Okay. You don't want to buy an animal.\n";
		}

		if(playerMoney <= 0)
		{
			cout << "You don't have enough money to keep playing. Game over!\n" << endl;
//		m.quitProgram();
			return 0;
		}

		// Get end of day input to quit or to continue
		bool endInput = m.userInputEndOfDay();

		if(endInput == true)
		{
			// Continue the game to the next day
			play = true;
		}
		else
		{
			// End the game
			play = false;
			return 0;
		}
	}



	return 0;


}


// Sickness function
void Zoo::sickness()
{
	cout << "An animal at the zoo is sick." << endl;
	int sickAnimal = 0;
	sickAnimal = rand() % 3 + 1;
	switch(sickAnimal)
	{
		case 1:
			if(tigerArr.getSize() == 0)
			{
				cout << "There was a tiger sickness in the area. But your zoo doesn't have any tigers "
					 <<	 "so you are safe." << endl;
			}
			else
			{
				cout << "A tiger is dead and being removed from the zoo." << endl;
				tigerArr.popBack();
				cout << "The number of tigers in the zoo now are: " << tigerArr.getSize() << endl;
			}
			break;
		case 2:
			if(penguinArr.getSize() == 0)
			{
				cout << "There was a penguin sickness in the area. But your zoo doesn't have any penguins "
					 <<	 "so you are safe." << endl;
			}
			else
			{
				cout << "A penguin is dead and being removed from the zoo." << endl;
				penguinArr.popBack();
				cout << "The number of penguins in the zoo now are: " << penguinArr.getSize() << endl;
			}
			break;
		case 3:
			if(turtleArr.getSize() == 0)
			{
				cout << "There was a turtle sickness in the area. But your zoo doesn't have any turtles "
					 <<	 "so you are safe." << endl;
			}
			else
			{
				cout << "A turtle is dead and being removed from the zoo." << endl;
				turtleArr.popBack();
				cout << "The number of turtles in the zoo now are: " << turtleArr.getSize() << endl;
			}
			break;
	}
}

// Boom function
void Zoo::boom()
{
	// Reset tiger bonus for the day to 0
	tigerBonus = 0;

	cout << "There is a boom in zoo attendance." << endl;
	cout << "You will earn $500 for each tiger you have!" << endl;
	cout << "The number of tigers you currently have is " << tigerArr.getSize() << endl;
	if(tigerArr.getSize() == 0)
	{
		cout << "Sorry. You have no tigers so you will not get a bonus." << endl;
	}
	else
	{
		tigerBonus = tigerArr.getSize() * 500;
		cout << "Congratulations! You have earned a bonus of $" << tigerBonus << endl;
	}

	// p&l
	// Tiger bonus being added to player money
	playerMoney += tigerBonus;
}

// Baby function
void Zoo::baby()
{
	cout << "Exciting! A new baby animal could be born in your zoo...." << endl;

	int animalToHaveBaby = 0;

	bool askAgain = true;

	bool checkedTigers = false;
	bool checkedPenguins = false;
	bool checkedTurtles = false;

	bool hadBaby = false;

	while(askAgain)
	{
		// Check to see whether tigers, penguins or turtles have been checked once
		if(checkedTigers && checkedPenguins && checkedTurtles)
		{
			askAgain = false;
			cout << "Unfortunately, there are no adult animals in the zoo currently "
					"that can have babies." << endl;
			break;
		}

		// Generate a random number
		animalToHaveBaby = rand() % 3 + 1;

		bool haveTigers = false;

		bool havePenguins = false;

		bool haveTurtles = false;

		// 1 for tiger
		if(animalToHaveBaby == 1 && !checkedTigers)
		{
			cout << "Baby time! Checking to see if there are any adult tigers "
					"who can have a baby..." << endl;

			cout << "Checking to see how many tigers there are. " << endl;

			if(tigerArr.getSize() > 0)
			{
				haveTigers = true;
				cout << "You currently have " << tigerArr.getSize() << " tiger(s) in the zoo." << endl;
			}
			else
			{
				haveTigers = false;
				cout << "You do not have any tigers at this zoo. Looking for another adult animal." << endl;
				askAgain = true;
			}

			if(haveTigers) // If you have tigers, then check if one is old enough
			{
				cout << "Checking if a tiger is old enough to have a baby." << endl;
				for (unsigned int i = 0; i < tigerArr.getSize(); ++i)
				{
					if(tigerArr[i].getAge() >= adultAge)
					{
						askAgain = false;
						cout << "You have a tiger that is old enough to have a baby." << endl;
						// Add a new tiger to the array with arguments
						addTiger(0, 0, 1, tigerFeedingCost, tigerPayOff);
						cout << "New tiger baby added!" << endl;
						hadBaby = true;
						break;
					}
				}
			}

			if(!hadBaby) // If a baby has been born no need to display the below message
			{
				cout << "There are no adult tigers in the zoo that can have babies." << endl;
			}
			cout << "\n\n";
			checkedTigers = true;

		} // Tiger end

		// 2 for penguin
		if(animalToHaveBaby == 2 && !checkedPenguins)
		{
			cout << "Baby time! Checking to see if there are any adult penguins "
					"who can have a baby..." << endl;

			cout << "Checking to see how many penguins there are. " << endl;

			if(tigerArr.getSize() > 0)
			{
				havePenguins = true;
				cout << "You currently have " << penguinArr.getSize() << " penguin(s) in the zoo." << endl;
			}
			else
			{
				havePenguins = false;
				cout << "You do not have any penguins at this zoo. Looking for another adult animal." << endl;
				askAgain = true;
			}

			if(havePenguins) // If you have penguins, then check if one is old enough
			{
				cout << "Checking if a penguin is old enough to have a baby." << endl;
				for (unsigned int i = 0; i < penguinArr.getSize(); ++i)
				{
					if(tigerArr[i].getAge() >= adultAge)
					{
						askAgain = false;
						cout << "You have a penguin that is old enough to have a baby." << endl;
						// Add new penguins to the array with arguments
						for (unsigned int j = 0; j < 5; ++j) {
							addPenguin(0, 0, 5, penguinFeedingCost, penguinPayOff);
							cout << "New penguin baby added!" << endl;
						}
						hadBaby = true;
						break;
					}
				}
			}

			if(!hadBaby) // If a baby has been born no need to display the below message
			{
				cout << "There are no adult penguins in the zoo that can have babies." << endl;
			}
			cout << "\n\n";
			checkedPenguins = true;

		} // Penguin end

		// 3 for turtle
		if(animalToHaveBaby == 3 && !checkedTurtles)
		{
			cout << "Baby time! Checking to see if there are any adult turtles "
					"who can have a baby..." << endl;

			cout << "Checking to see how many turtles there are. " << endl;

			if(turtleArr.getSize() > 0)
			{
				haveTurtles = true;
				cout << "You currently have " << turtleArr.getSize() << " turtle(s) in the zoo." << endl;
			}
			else
			{
				haveTurtles = false;
				cout << "You do not have any turtles at this zoo. Looking for another adult animal." << endl;
				askAgain = true;
			}

			if(haveTurtles) // If you have turtles, then check if one is old enough
			{
				cout << "Checking if a turtle is old enough to have a baby." << endl;
				for (unsigned int i = 0; i < turtleArr.getSize(); ++i)
				{
					if(turtleArr[i].getAge() >= adultAge)
					{
						askAgain = false;
						cout << "You have a turtle that is old enough to have a baby." << endl;
						// Add new turtles to the array with arguments
						for (unsigned int j = 0; j < 10; ++j) {
							addTurtle(0, 0, 10, turtleFeedingCost, turtlePayOff);
							cout << "New turtle baby added!" << endl;
						}
						hadBaby = true;
						break;
					}
				}
			}
			if(!hadBaby) // If no animal has had a baby, then display this
			{
				cout << "There are no adult turtles in the zoo that can have babies." << endl;
			}

			cout << "\n\n";
			checkedTurtles = true;
		} // Turtles end
	}
}


// Random events
void Zoo::randomEvent()
{
	int result = 0;

	// Calculate a random number from 1 to N
	result = rand() % 4 + 1;

	// Pick a random event
	switch(result)
	{
		case 1: // Sickness
		{
			sickness();
			break;
		} // sickness end
		case 2: // boom
		{
			boom();
			break;
		}
		case 3: // Baby animal born
		{
			baby();
			break;
		}
		case 4:
		{
			cout << "Nothing happened in today's random event." << endl;
			break;
		}
		default:
		{
			cout << "This should not happen." << endl;
			break;
		}
	}
	cout << "\n\n\n";
}

// End of day buying
bool Zoo::endOfDayBuying()
{
	int endOfDayBuy = m.userInputBuyAnimal();


	if(endOfDayBuy == 0)
	{
		return false;
	}
	else
	{
		switch (endOfDayBuy)
		{
			case 1: // Buy tiger
			{

				// Add tiger to array with arguments
				addTiger(3, tigerCost, numBabiesTiger, tigerFeedingCost, tigerPayOff);
				cout << "A 3 day old tiger was added to the zoo." << endl;
				cout << "Subtracting the cost of the tiger $" << tigerCost << " from your bank." << endl;
				// p&l
				playerMoney -= tigerCost;
				cout << "Money left in the bank $" << playerMoney << endl;
				cout << "You have the following tigers in the zoo now : " << endl;
				printTigerArr();
				return true;
				break;
			}
			case 2: // Buy penguin
			{
				// Add penguin to array with arguments
				addPenguin(3, penguinCost, numBabiesPenguin, penguinFeedingCost, penguinPayOff);
				cout << "A 3 day old penguin was added to the zoo." << endl;
				cout << "Subtracting the cost of the penguin $" << penguinCost << " from your bank." << endl;
				// p&l
				playerMoney -= penguinCost;
				cout << "Money left in the bank $" << playerMoney << endl;
				cout << "You have the following penguins in the zoo now : " << endl;
				printPenguinArr();
				return true;
				break;
			}
			case 3: // Buy turtle
			{
				// Add turtle to array with arguments
				addTurtle(3, turtleCost, numBabiesTurtle, turtleFeedingCost, turtlePayOff);
				cout << "A 3 day old turtle was added to the zoo." << endl;
				cout << "Subtracting the cost of the turtle $" << turtleCost << " from your bank." << endl;
				//p&l
				playerMoney -= turtleCost;
				cout << "Money left in the bank $" << playerMoney << endl;
				cout << "You have the following turtles in the zoo now : " << endl;
				printTurtleArr();
				return true;
				break;
			}
			case 4: // No buying continue to next day
			{
				cout << "Okay... you don't want to add any animals to the zoo." << endl;
				return false;
				break;
			}
			default: {
				cout << "This should not happen." << endl;
				return false;
				break;
			}
		}
	}



}

// Display profit and loss
// Before end of day buying of animals
void Zoo::displayProfitLoss()
{
	// p&l
	// At this point all subtractions from player money have been made for day 0
	// This is before end of day buying of animals
	// Have the subtractions for feeding costs been made for the day? Yes in payFeedingCosts()
	// Has the tiger bonus been added to the playerMoney? Yes in the boom() method.
	// Has the daily payoff been added to the playerMoney?

	// Profit and loss
	cout << "Your financials at the end of day " << days << " are: " << endl;
	cout << "Today's payoff $" << dailyPayOff << endl;
	cout << "Today's bonus $" << tigerBonus << endl;
	cout << "Daily feeding costs $" << dailyFeedingCost << endl;
	cout << "Player money $" << playerMoney << endl;
	cout << "Your profit/loss at the end of the day " << days << " is $" << dailyProfitLoss
		 << endl;

	cout << "\n\n" << endl;
}

// Pay feeding costs
void Zoo::payFeedingCosts()
{
	// Pay the feeding cost of each animal
	cout << "Paying the feeding costs for all the animals..." << endl;
	// Reset the daily feeding cost to 0 each day
	dailyFeedingCost = 0;

	// Tigers
	for(size_t i = 0; i < tigerArr.getSize(); i++)
	{
		dailyFeedingCost += tigerArr[i].getBaseFoodCost();
	}
	// Penguins
	for(size_t i = 0; i < penguinArr.getSize(); i++)
	{
		dailyFeedingCost += penguinArr[i].getBaseFoodCost();
	}

	// Turtles
	for(size_t i = 0; i < turtleArr.getSize(); i++)
	{
		dailyFeedingCost += turtleArr[i].getBaseFoodCost();
	}

	cout << "Player money before subtracting daily feeding costs: $" << playerMoney << endl;

	// User pays for feeding costs for each animal
	// p&l
	playerMoney -= dailyFeedingCost;

	cout << "Total feeding cost for the day: $" << dailyFeedingCost << endl;
	cout << "Player money after subtracting daily feeding costs: $" << playerMoney << endl;
	cout << "\n\n\n";
}

// Increase the age of all animal
void Zoo::increaseAge()
{
	// Increase the age of all animals by 1
	cout << "Increasing the age of all animals in the zoo by 1 day." << endl;
	// Tigers
	for(size_t i = 0; i < tigerArr.getSize(); i++)
	{
		tigerArr[i].increaseAge();
	}
	// Penguins
	for(size_t i = 0; i < penguinArr.getSize(); i++)
	{
		penguinArr[i].increaseAge();
	}

	// Turtles
	for(size_t i = 0; i < turtleArr.getSize(); i++)
	{
		turtleArr[i].increaseAge();
	}
}

// Display morning game stats
void Zoo::displayGameStatsMorning()
{
	// The game starts in the morning
	cout << "Stats at the start of day: " << days + 1 << endl;
	cout << "Please see below all the tigers that you have: " << endl;
	cout << "You have a total of " << tigerArr.getSize() << " tigers" << endl;
	printTigerArr();
	cout << "\n\n";
	cout << "Please see below all the penguins that you have: " << endl;
	cout << "You have a total of " << penguinArr.getSize() << " penguins" << endl;
	printPenguinArr();
	cout << "\n\n";
	cout << "Please see below all the turtles that you have: " << endl;
	cout << "You have a total of " << turtleArr.getSize() << " turtles" << endl;
	printTurtleArr();
	cout << "\n\n";
}

// Increase day
void Zoo::increaseDay()
{
	days++;
}

// Play game
int Zoo::playGame()
{
	// Ask the user if he/she wants to play or quit?
	bool initialChoice = m.getUserInput();
	if(initialChoice == false)
	{
		play = false;
		return 0;
	}
	else
	{
		cout << "Welcome to the Zoo Tycoon game..." << endl;
		cout << "Loading your bank account with $100,000 and buying a "
				"tiger, a penguin, and a turtle to get you started." << endl;
		cout << "\n\n\n";
		// Show the user the initial stats for the game:
		cout << "After the purchase of 1 tiger, 1 penguin, and 1 turtle you have: $" << playerMoney << " left." << endl;
		cout << "Here are some key metrics that you want to keep an eye on: " << endl;
		cout << "Current daily feeding costs: $" << dailyFeedingCost << endl;
		cout << "Subtracting the daily feeding costs from the player money..." << endl;
		// p&l
		playerMoney -= dailyFeedingCost;
		cout << "Player money after subtracting daily feeding costs for day 0: $" << playerMoney << endl;
		cout << "Current daily pay off: $" << dailyPayOff << endl;
		cout << "\n\n\n";
	}

	while(play)
	{
		turn();
	}

	return 0;
}


// Calc daily payoff
void Zoo::calcDailyPayoff()
{
	// Calculate the daily payoff of all the tigers, penguins, and turtles
	// that are in the zoo

	// Set the variable to 0 at the start of the calculation
	dailyPayOff = 0;

	// Tigers
	for (unsigned int i = 0; i < tigerArr.getSize(); ++i) {
		dailyPayOff += tigerArr[i].getPayOff();
	}

	// Penguins
	for (unsigned int j = 0; j < penguinArr.getSize(); ++j) {
		dailyPayOff += penguinArr[j].getPayOff();
	}

	// Turtles
	for (unsigned int k = 0; k < turtleArr.getSize(); ++k) {
		dailyPayOff += turtleArr[k].getPayOff();
	}

	// p&l
	// dailyPayOff is added to the player money
	playerMoney += dailyPayOff;
}

// Calc daily profit/loss
// This is before the end of the day buying of animals
void Zoo::calcDailyProfitLoss()
{
	// Calculation of daily profit and loss
	// Revenue

	// Set dailyProfitLoss to 0
	dailyProfitLoss = 0;

	dailyProfitLoss = dailyPayOff + tigerBonus - dailyFeedingCost;
}

// Reset variables
void Zoo::resetVars()
{
	tigerBonus = 0;
	dailyProfitLoss = 0;
	dailyPayOff = 0;
}

// Day separator
void Zoo::daySeparator()
{
	cout << "-----------------------------------------" << endl;
	cout << "\n\n\n";
}