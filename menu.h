/*********************************************************************
** Program name: menu.h
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Menu class specification file.
** Implements the menu of the game
*********************************************************************/

#ifndef ZOOTYCOON_MENU_H
#define ZOOTYCOON_MENU_H

#include <string>
#include <iostream>
#include <iomanip> // for input validation to take in only one value
#include <climits> // for ignoring the characters (large number)
using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::setw;

class Menu
{

private:
	int initialInput;
	int buyAnimalInput;
	int endOfDayInput;

public:
	// Constructor
	Menu();

	// Get the user choice for all inputs needed
	int getChoiceVar(string inputMessage, int minNum, int maxNum, bool extraError, bool quitFlag, bool animalBuyQuitFlag);

	// Get user input
	bool getUserInput();

	// Quit the program
	void quitProgram();

	// Get input for buying animal
	int userInputBuyAnimal();

	// Getter
	int getBuyAnimalInput();

	// End of day
	bool userInputEndOfDay();

	// Getter
	int getEndOfDayInput();
};


#endif //ZOOTYCOON_MENU_H
