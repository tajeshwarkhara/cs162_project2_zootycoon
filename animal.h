/*********************************************************************
** Program name: animal.h
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Specification file for the Animal class.
** Animal is the parent class to all the animals in the zoo.
*********************************************************************/
#ifndef ZOOTYCOON_ANIMAL_H
#define ZOOTYCOON_ANIMAL_H
#include <ostream>
using std::ostream;

class Animal
{
private:
protected:
	// Making these protected so that they can be accesses directly
	// by the derived/child classes.
	int age;
	double cost;
	int numberOfBabies;
	double baseFoodCost;
	double payOff;
public:
	// Default constructor
	Animal();

	// Optional constructor
	Animal(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// Copy constructor
	Animal(const Animal& obj);

	// Overloading the = operator
	Animal& operator=(const Animal& right);

	// Destructor
	~Animal();

	// Overloaded stream insertion operator for printing objects
	friend ostream &operator<<(ostream& out, Animal a);

	// Increase age
	void increaseAge();

	// Getters
	double getBaseFoodCost();
	int getAge();

	double getPayOff();


};


#endif //ZOOTYCOON_ANIMAL_H
