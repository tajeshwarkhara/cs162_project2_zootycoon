/*********************************************************************
** Program name: Penguin.cpp
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Implementation file for Penguin class.
** Child of the Animal class.
*********************************************************************/
#include "penguin.h"

// Default constructor
Penguin::Penguin()
		: Animal(1, 1000, 5, 10, 100)
{
	// If a Penguin object is created using a default constructor
	// with no arguments then initialize the following values for
	// that object as default
	// age = 0, cost = 1000, nuBabies = 5, baseFoodCost = 10, payOff = 1
}

// Optional constructor
Penguin::Penguin(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
		: Animal(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn)
{
	// Initialized custom values sent in through the constructor
}

// Destructor
Penguin::~Penguin()
{
	// Deallocate any used dynamic memory here
}

// Accessors
int Penguin::getAge()
{
	return age;
}
double Penguin::getCost()
{
	return cost;
}
double Penguin::getNumBabies()
{
	return numberOfBabies;
}
double Penguin::getBaseFoodCost()
{
	return baseFoodCost;
}
double Penguin::getPayOff()
{
	return payOff;
}