/*********************************************************************
** Program name: Tiger.cpp
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Implementation file for Tiger class.
** Child of the Animal class.
*********************************************************************/
#include "tiger.h"

// Default constructor
Tiger::Tiger()
	: Animal(1, 10000, 1, 50, 2000)
{
	// If a Tiger object is created using a default constructor
	// with no arguments then initialize the following values for
	// that object as default
	// age = 1, cost = 10,000, numBabies = 1, baseFoodCost = 50, payOff = 10
}

// Optional constructor
Tiger::Tiger(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
	: Animal(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn)
{
	// Initialized custom values sent in through the constructor
}

// Destructor
Tiger::~Tiger()
{

}

// Accessors
int Tiger::getAge()
{
	return age;
}
double Tiger::getCost()
{
	return cost;
}
double Tiger::getNumBabies()
{
	return numberOfBabies;
}
double Tiger::getBaseFoodCost()
{
	return baseFoodCost;
}
double Tiger::getPayOff()
{
	return payOff;
}
