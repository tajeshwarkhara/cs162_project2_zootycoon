/*********************************************************************
** Program name: Tiger.h
** Author: Tajeshwar Singh Khara
** Date: 11-10-2018
** Description: Specification file for Tiger class.
** Child of the Animal class.
*********************************************************************/
#ifndef ZOOTYCOON_TIGER_H
#define ZOOTYCOON_TIGER_H

#include "animal.h"


class Tiger : public Animal
{

public:
	// Default constructor
	Tiger();

	// Optional constructor
	Tiger(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn);

	// Accessors
	int getAge();
	double getCost();
	double getNumBabies();
	double getBaseFoodCost();
	double getPayOff();

	// Destructor
	~Tiger();

};


#endif //ZOOTYCOON_TIGER_H
