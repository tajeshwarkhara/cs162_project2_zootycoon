/*********************************************************************
** Program name: AnimalArray.cpp
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Implementation file for AnimalArray class
*********************************************************************/

#include <cstdio>
#include "AnimalArray.h"

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called AnimalArray::AnimalArray()
** Constructor
** Initialize the new array object to a capacity of 10, valid elements to 0,
** and dynamically allocate memory for the Animal pointers.
******************************************************************************/
AnimalArray::AnimalArray()
{
	cap = 10;
	nrOfEl = 0;
	// Array of 10 animals created
	arr = new Animal*[cap];

	// Initialize
	for (unsigned int i = 0; i < cap; ++i) {
		arr[i] = nullptr;
	}
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called AnimalArray::~AnimalArray()
** Destructor
** Deallocate dynamically allocated memory during the life of the object.
******************************************************************************/
AnimalArray::~AnimalArray()
{
	for(size_t i = 0; i < cap; i++)
	{
		delete arr[i];
	}

	delete[] arr;
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called Animal** AnimalArray::getArr()
** Getter
** Returns the pointer pointer array. This is mostly used for debugging.
******************************************************************************/
Animal** AnimalArray::getArr()
{
	return arr;
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called int AnimalArray::getSize()
** Getter
** Return number of valid elements in the AnimalArray object.
******************************************************************************/
unsigned int AnimalArray::getSize()
{
	return nrOfEl;
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called void AnimalArray::expand()
** Doubles the capacity of the array when all spaces are occupied.
******************************************************************************/
void AnimalArray::expand()
{
	//1: double cap
	cap *= 2;

	//2: Create tempArr with new size
	Animal** tempArr = new Animal*[cap];

	//3: Initialize
	for (size_t i = 0; i < cap; ++i) {
		tempArr[i] = nullptr;
	}

	//4: Copy over stuff from old one
	for(size_t i = 0; i < nrOfEl; i++)
	{
		tempArr[i] = new Animal(*arr[i]); // Uses default copy constructor
	}

	//5: Deallocate memory
	for(size_t i = 0; i < nrOfEl; i++)
	{
		delete arr[i];
	}

	//6: Delete the old array
	delete[] arr;

	//7: Set old array to new one
	arr = tempArr;

	//8: Initialize
	this->initialize(this->nrOfEl);

}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called void AnimalArray::add(Animal el)
** Adds an animal object to the array.
******************************************************************************/
void AnimalArray::add(Animal el)
{
	if(nrOfEl >= cap)
	{
		expand();
	}

	arr[nrOfEl++] = new Animal(el); // Default assignment operator
}

// Add an animal object to the array
void AnimalArray::add(int ageIn, double costIn, int numBabiesIn, double baseFoodCostIn, double payOffIn)
{

	Animal a = Animal(ageIn, costIn, numBabiesIn, baseFoodCostIn, payOffIn);
	add(a);
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called Animal AnimalArray::getAt(int index) const
** Returns the specified object based on index.
******************************************************************************/
Animal& AnimalArray::getAt(unsigned int index) const
{
	if (index < 0 || index >= nrOfEl)
		throw ("Out of bounds exception!");

	return *arr[index];
}

// To return the pointer in the Array
Animal* AnimalArray::getAtPointer(unsigned int index)
{
	if (index < 0 || index >= nrOfEl)
		throw ("Out of bounds exception!");

	return arr[index];
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called void AnimalArray::popBack()
** Reduces the number of elements by 1 and deallocates the memory of the
** last pointer in the array.
******************************************************************************/
void AnimalArray::popBack()
{
	if(nrOfEl > 0)
	{
		nrOfEl--;
	}
	else
	{
		throw("Empty array!");
	}
}

/****************************************************************************
** Author: Tajeshwar Singh Khara
** Date: 12-10-2018
** Description: Function called void AnimalArray::print()
** Prints out the array starting from 0 to the last valid element.
******************************************************************************/
void AnimalArray::print()
{
	for(size_t i = 0; i < nrOfEl; i++)
	{
		cout << "i: " << i << endl;
		cout << getAt(i) << endl;
	}
}

// To return the selected element form the array
Animal& AnimalArray::operator[](unsigned int index)
{
	if (index < 0 || index >= nrOfEl)
		throw ("Out of bounds exception!");

	return *arr[index];
}

// Initialize
void AnimalArray::initialize(int from)
{
	for (size_t i = from; i < this->cap; i++)
	{
		this->arr[i] = nullptr;
	}
}
